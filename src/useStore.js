import { store$ } from "./store";
import { useEffect, useState } from "react";

function useStore(stateToUse, defaultValue = []) {

    const [ state, setState ] = useState(defaultValue)

    useEffect(() => {
        const sub$ = store$.subscribe(data => {
            setState(data[stateToUse])
        })

        return () =>  sub$.unsubscribe()
    },[stateToUse])

    return state
}

export default useStore
