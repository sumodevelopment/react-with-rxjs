import './App.css';
import MovieList from "./Movie/MovieList";
import Navbar from "./Navbar/Navbar";
import React from "react";

function App() {
    return (
        <div className="App">
            <Navbar/>
            <MovieList/>
        </div>
    );
}

export default App;
