import React, { useEffect } from 'react'
import MovieListItem from "./MovieListItem";
import { dispatcher$ } from "../store";
import useStore from "../useStore";

const MovieList = () => {

    const movies = useStore('movies' )

    useEffect(() => {
        dispatcher$.next({ action: 'GET_MOVIES' })
    }, [])

    // unchanged JSX.
    return (
        <main>
            <ul>
                { movies.map(movie =>
                    <MovieListItem key={ movie.id } movie={movie} />
                )}
            </ul>
        </main>
    )
}

export default MovieList
