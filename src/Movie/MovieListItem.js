import styles from './MovieItem.module.css'
import { dispatcher$ } from "../store";
import React from "react";

const MovieListItem = ({ movie }) => {

    // Delete a movie.
    const onMovieDelete = () => {
        if (window.confirm('Are you sure?')) {
            dispatcher$.next({ action: 'DELETE_MOVIE', payload: movie.id })
        }
    }

    return (
        <li onClick={ onMovieDelete } className={ styles.MovieItem }>
            <div className={ styles.MovieItemCover }>
                <img src={ movie.cover } alt={ movie.title } />
            </div>
            <div className={ styles.MovieItemDetails }>
                <h4 className={ styles.MovieItemTitle }>{ movie.title }</h4>
                <p>{ movie.description }</p>
            </div>
        </li>
    )
}

export default MovieListItem
