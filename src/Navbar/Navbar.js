import { dispatcher$ } from "../store";
import useStore from "../useStore";
import React from "react";

const Navbar = () => {

    const movieCount = useStore('movies').length

    const onClearMovies = () => {
        if (window.confirm('Are you sure?')) {
            dispatcher$.next({ action: 'CLEAR_MOVIES' })
        }
    }

    return (
        <nav>
            <ul>
                <li>Number of movies { movieCount }</li>
            </ul>
            <button onClick={ onClearMovies }>Clear movies</button>
        </nav>
    )
}

export default Navbar
