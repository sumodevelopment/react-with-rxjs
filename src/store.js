import {Subject} from "rxjs";

let AppState = {
    movies: []
}

export const store$ = new Subject();
export const dispatcher$ = new Subject()

dispatcher$.subscribe(data => {
    switch (data.action) {
        case 'GET_MOVIES':
            fetch('http://localhost:5000/movies')
                .then(r => r.json())
                .then(movies => {
                    AppState = {
                        ...AppState,
                        movies
                    }
                    return AppState
                })
                .then(state => store$.next(state))
            break
        case 'CLEAR_MOVIES':
            AppState = {
                ...AppState,
                movies: []
            }
            store$.next( AppState )
            break
        case 'DELETE_MOVIE':
            AppState = {
                ...AppState,
                movies: AppState.movies.filter( movie => movie.id !== data.payload )
            }
            store$.next( AppState )
            break
        case 'ADD_MOVIE':
            AppState = {
                movies: [ ...AppState.movies, data.payload ]
            }
            store$.next( AppState )
            break
        default:
            store$.next( AppState )
            break
    }
})
